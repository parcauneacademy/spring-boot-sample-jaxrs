package com.parcaune.academy.tutorial.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

@Component
@Path("/ping")
public class PingEndpoint {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String ping() {
		return "Sample JAX-RS Up and Running!";
	}

	@GET
	@Path("/{whoami}")
	@Produces(MediaType.TEXT_PLAIN)
	public String pingWithMessage(@PathParam("whoami") String whoami) {
		return "Welldone " + whoami;
	}

}