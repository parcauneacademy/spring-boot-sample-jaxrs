package com.parcaune.academy.tutorial.jaxrs;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String index() {
		return "Greetings from  Willie !";
	}

	@RequestMapping("/saymyname")
	public String saymyname() {
		return "<html><body><h1>Willie ther we go</h1></body></html>";
	}

}